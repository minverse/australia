var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

// define routes
var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var adminPanel = require('./routes/admin');
var anythingGoes = require('./routes/agc');
var seriousDiscussion = require('./routes/sdc');
var agcPopular = require('./routes/agcpopular');
var loginPage = require('./routes/login');
var signupPage = require('./routes/signup');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'twig');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use('/s', express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/users', usersRouter);

// define minverse stuff
app.use('/admin_panel', adminRouter);
app.use('/login', loginPage);
app.use('/signup', signuPage);

// define communities and other things (based on my current understanding of express routing)
app.use('/titles/1', anythingGoes);
app.use('/titles/2', seriousDiscussion);
app.use('/titles/1/popular', agcPopular);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
